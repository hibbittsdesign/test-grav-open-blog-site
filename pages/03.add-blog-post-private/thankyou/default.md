---
title: 'Thank You'
external_links:
    process: true
    title: false
    no_follow: true
    target: _blank
    mode: active
---

Thank you, your submitted page has been [published](../../blog).
