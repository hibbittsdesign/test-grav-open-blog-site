---
title: 'Add Blog Post (Password Required)'
taxonomy:
    tag: hidden
parent: /blog
pagefrontmatter:
    template: item
    published: true
form:
  name: add-page-form-password

  fields:
    - name: title
      label: 'Title of Blog Post'
      placeholder: null
      autocomplete: true
      type: text
      validate:
        required: true
    - name: author
      label: 'Author of Blog Post'
      placeholder: null
      type: text
      validate:
        required: true
    - name: content
      label: 'Content of Blog Post'
      size: long
      placeholder: null
      type: textarea
      id: simplemde
      validate:
        required: true
    - name: taxonomy.tag
      type: checkboxes
      label: 'Tag for Blog Post'
      options:
        tag1: 'Tag 1'
        tag2: 'Tag 2'
        tag3: 'Tag 3'
        tag4: 'Tag 4'
  buttons:
    - type: submit
      value: Submit Blog Post
      classes: null
  process:
    - addpage: null
    - display: thankyou
---

Enter the title, content (formatted in [Markdown](https://simplemde.com/markdown-guide)) and choose the category tag for the new blog post page.
