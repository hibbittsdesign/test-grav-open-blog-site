---
title: 'Open Blogging as easy as 1-2-3'
date: '05-01-2017 00:00'
taxonomy:
    category:
        - blog
    tag:
        - howto
summary:
    enabled: '0'
author: 'Paul Hibbitts'
---

Want to start open blogging with Grav?

1. Run the [setup wizard for the GitSync Plugin](../../admin/plugins/git-sync) and follow the provided instructions.
2. Copy the URL for your Git repository when viewing or editing a file in the 'pages' folder and configure the ['View/Edit Blog Post in Git Link' settings](../../admin/config/site).
3. You are ready to go, enjoy!
